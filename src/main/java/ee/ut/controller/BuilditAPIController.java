package ee.ut.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.sun.jersey.api.client.ClientResponse;

import ee.ut.controller.buildits.BuilditController;
import ee.ut.controller.buildits.BuilditControllerFabric;
import ee.ut.domain.PurchaseOrder;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.RejectBean;

@Controller
public class BuilditAPIController {

	// static private String credentials = "rentit:rentit";
	// static private String server = Servers.buildit;

	static public void acceptPHR(Long id) {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		String server = po.getServer();
		BuilditController controller = BuilditControllerFabric.getController(server);
		controller.acceptPHR(id);
	}

	static public void rejectPHR(RejectBean bean)
			throws JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(bean.getId());
		String server = po.getServer();
		BuilditController controller = BuilditControllerFabric.getController(server);
		controller.rejectPHR(bean);
	}
	
	static public void rejectPHRExtension(RejectBean bean)
			throws JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(bean.getId());
		String server = po.getServer();
		BuilditController controller = BuilditControllerFabric.getController(server);
		controller.rejectExtensionPHR(bean);
	}

	public static void deliverPlant(Long id) throws JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		String server = po.getServer();
		BuilditController controller = BuilditControllerFabric.getController(server);
		controller.deliverPlant(id);

	}

	public static void returnPlant(Long id) throws JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		String server = po.getServer();
		BuilditController controller = BuilditControllerFabric.getController(server);
		controller.returnPlant(id);

	}

	public static void rejectPlant(Long id) throws JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		String server = po.getServer();
		BuilditController controller = BuilditControllerFabric.getController(server);
		controller.rejectPlant(id);
	}
}
