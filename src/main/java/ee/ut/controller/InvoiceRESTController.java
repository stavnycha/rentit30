package ee.ut.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import ee.ut.domain.Invoice;
import ee.ut.exceptions.*;
import ee.ut.rest.InvoiceResource;
import ee.ut.rest.InvoiceResourceAssembler;
import ee.ut.service.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/rest/invoice")
public class InvoiceRESTController {

	@Autowired
	private PurchaseOrderService poService;

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/accept")
	public ResponseEntity<InvoiceResource> acceptInvoice(@PathVariable("id") Long id)
			throws JsonProcessingException {
		Invoice i = null;
		ResponseEntity<InvoiceResource> response = null;
		try {
			i = poService.acceptInvoice(id);
			InvoiceResourceAssembler assembler = new InvoiceResourceAssembler();
			response = new ResponseEntity<>(
					assembler.toResource(i), HttpStatus.OK);
		} catch (PuchasedOrderNotFound e) {
			response = new ResponseEntity<>(
					HttpStatus.NOT_FOUND);
			e.printStackTrace();
		} catch (MethodNotAllowedException e) {
			response = new ResponseEntity<>(
					HttpStatus.BAD_REQUEST);
			e.printStackTrace();
		}

		return response;
	}

	@ExceptionHandler({ PlantUnavailableException.class,
			InvalidHirePeriodException.class })
	public ResponseEntity<String> handleBadRequest(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ PuchasedOrderNotFound.class,
			UpdateRequestNotFound.class })
	public ResponseEntity<String> handleNotFound(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler({ SecurityException.class })
	public ResponseEntity<String> handleSecurity(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler({ NoSuchMethodException.class })
	public ResponseEntity<String> handleNoMethod(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_IMPLEMENTED);
	}

	@ExceptionHandler({ MethodNotAllowedException.class })
	public ResponseEntity<String> handleMethodNotAllowed(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(),
				HttpStatus.METHOD_NOT_ALLOWED);
	}

}
