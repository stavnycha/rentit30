package ee.ut.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ee.ut.domain.Plant;
import ee.ut.exceptions.PlantNotFoundException;
import ee.ut.repository.PlantRepository;
import ee.ut.rest.PlantResource;
import ee.ut.rest.PlantResourceAssembler;
import ee.ut.rest.PlantResourceList;
import ee.ut.rest.QueriedPlant;
import ee.ut.service.PlantService;

@Controller
@RequestMapping("/rest/plants")
public class PlantRESTController {

	@Autowired
	private PlantService plantService;

	@Autowired
	PlantRepository plantRepo;

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<PlantResource> getPlant(@PathVariable Long id)
			throws PlantNotFoundException {
		Plant plant = plantService.getPlant(id);

		PlantResourceAssembler assember = new PlantResourceAssembler();
		ResponseEntity<PlantResource> response = new ResponseEntity<>(
				assember.toResource(plant), HttpStatus.OK);

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "")
	public ResponseEntity<PlantResourceList> getAllPlants() {
		List<Plant> pos = Plant.findAllPlants();

		PlantResourceAssembler assembler = new PlantResourceAssembler();
		PlantResourceList resList = assembler.toResource(pos);

		ResponseEntity<PlantResourceList> response = new ResponseEntity<>(
				resList, HttpStatus.OK);

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/available")
	public ResponseEntity<PlantResourceList> getAvailablePlants(
			@RequestParam(value = "name", required = true) String name, 
			@RequestParam(value = "start", required = true) String start, 
			@RequestParam(value = "end", required = true) String end) {

		QueriedPlant plant = new QueriedPlant();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		try {
			plant.setStartDate(format.parse(start));
			plant.setEndDate(format.parse(end));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		plant.setName(name);
		
		PlantResourceAssembler assembler = new PlantResourceAssembler();

		/*
		 * List<Plant> pos = Plant.findAllPlants();
		 * 
		 * PlantResourceAssembler assembler = new PlantResourceAssembler();
		 * PlantResourceList resList = assembler.toResource(pos);
		 */

		ResponseEntity<PlantResourceList> response = new ResponseEntity<>(
				assembler.toResource(plantRepo.findByNameAndAvailability(plant
						.getName(), plant.getStartDate(), plant
						.getEndDate())), HttpStatus.OK);

		return response;
	}

	@RequestMapping(method = RequestMethod.POST, value = "")
	public ResponseEntity<Void> createPlantResource(
			@RequestBody PlantResource res) {
		Plant p = plantService.createPlant(res);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(linkTo(PurchaseOrderRESTController.class).slash(
				p.getId()).toUri());

		ResponseEntity<Void> response = new ResponseEntity<>(headers,
				HttpStatus.CREATED);
		return response;
	}

	@ExceptionHandler({ PlantNotFoundException.class })
	public ResponseEntity<String> handleNotFound(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
	}

}
