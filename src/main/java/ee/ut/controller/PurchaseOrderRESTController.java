package ee.ut.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.lang.reflect.Method;
import java.net.URI;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ee.ut.domain.Invoice;
import ee.ut.domain.InvoiceStatus;
import ee.ut.domain.POStatus;
import ee.ut.domain.PurchaseOrder;
import ee.ut.exceptions.InvalidHirePeriodException;
import ee.ut.exceptions.MethodNotAllowedException;
import ee.ut.exceptions.PlantUnavailableException;
import ee.ut.exceptions.PuchasedOrderNotFound;
import ee.ut.exceptions.UpdateRequestNotFound;
import ee.ut.rest.InvoiceResourceAssembler;
import ee.ut.rest.InvoiceResource;
import ee.ut.rest.POModification;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.PurchaseOrderResourceAssembler;
import ee.ut.rest.PurchaseOrderResourceList;
import ee.ut.rest.RejectBean;
import ee.ut.service.PurchaseOrderService;
import ee.ut.utils.ExtendedLink;

@Controller
@RequestMapping("/rest/po")
public class PurchaseOrderRESTController {

	@Autowired
	private PurchaseOrderService poService;

    @Deprecated
	@RequestMapping(method = RequestMethod.GET, value = "/acceptInvoice/{id}")
	public ResponseEntity<InvoiceResource> acceptInvoice(@PathVariable Long id)
			throws JsonProcessingException {
		Invoice i = null;
		ResponseEntity<InvoiceResource> response = null;
		try {
			i = poService.acceptInvoice(id);
			InvoiceResourceAssembler assembler = new InvoiceResourceAssembler();
			response = new ResponseEntity<>(
					assembler.toResource(i), HttpStatus.OK);
		} catch (PuchasedOrderNotFound e) {
			response = new ResponseEntity<>(
					HttpStatus.NOT_FOUND);
			e.printStackTrace();
		} catch (MethodNotAllowedException e) {
			response = new ResponseEntity<>(
					HttpStatus.BAD_REQUEST);
			e.printStackTrace();
		}

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<PurchaseOrderResource> getPurchaseOrder(
			@PathVariable Long id) throws PuchasedOrderNotFound,
			NoSuchMethodException, SecurityException {

		PurchaseOrder po = poService.getPO(id);

		PurchaseOrderResourceAssembler assember = new PurchaseOrderResourceAssembler();
		PurchaseOrderResource resource = assember.toResource(po);

		Method _requestUpdatePO = PurchaseOrderRESTController.class.getMethod(
				"requestUpdatePO", PurchaseOrderResource.class);

		String requpdateLink = linkTo(_requestUpdatePO, po.getId()).toUri()
				.toString();
		resource.add(new ExtendedLink(requpdateLink, "Request Update PO",
				"POST"));

		ResponseEntity<PurchaseOrderResource> response = new ResponseEntity<>(
				resource, HttpStatus.OK);
		return response;

	}

	@RequestMapping(method = RequestMethod.GET, value = "")
	public ResponseEntity<PurchaseOrderResourceList> getAllPurchaseOrders() {
		List<PurchaseOrder> po = PurchaseOrder.findAllPurchaseOrders();

		PurchaseOrderResourceAssembler assembler = new PurchaseOrderResourceAssembler();
		PurchaseOrderResourceList resList = assembler.toResource(po);

		ResponseEntity<PurchaseOrderResourceList> response = new ResponseEntity<>(
				resList, HttpStatus.OK);

		return response;
	}
	

	@RequestMapping(method = RequestMethod.POST, value = "")
	public ResponseEntity<PurchaseOrderResource> createPurchaseOrder(
			@RequestBody PurchaseOrderResource res) {

		HttpHeaders headers = new HttpHeaders();
		PurchaseOrder po = null;
		ResponseEntity<PurchaseOrderResource> response = null;
		PurchaseOrderResourceAssembler assembler = new PurchaseOrderResourceAssembler();
		try {
			po = poService.createPO(res);
			headers.setLocation(linkTo(PurchaseOrderRESTController.class)
					.slash(po.getId()).toUri());
			response = new ResponseEntity<>(assembler.toResource(po),
					HttpStatus.CREATED);
		} catch (PlantUnavailableException e) {
			response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			e.printStackTrace();
		} catch (InvalidHirePeriodException e) {
			response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			e.printStackTrace();
		}

		return response;
	}
	
	
	/*
	 * API 1.1
	 */
	
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/modify")
    public ResponseEntity<PurchaseOrderResource> modifyPurchaseOrder(
    		@PathVariable Long id, @RequestBody PurchaseOrderResource request)
            throws PuchasedOrderNotFound, MethodNotAllowedException {
    	return modifyPO(request);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/extend")
    public ResponseEntity<PurchaseOrderResource> extendPurchaseOrder(
    		@PathVariable Long id, @RequestBody PurchaseOrderResource request) throws JsonProcessingException{
    	return requestUpdatePO(request);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Void> cancelPurchaseOrder(@PathVariable Long id) {
    	cancelPO(id);
    	ResponseEntity<Void> res = new ResponseEntity<>(HttpStatus.OK);
    	return res;
    }

    
    /*
     * API 1.0
     */
    
    @RequestMapping(method = RequestMethod.GET, value = "/cancel/{id}")
	public ResponseEntity<PurchaseOrderResource> cancelPO(@PathVariable Long id) {
		PurchaseOrderResourceAssembler assembler = new PurchaseOrderResourceAssembler();
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		ResponseEntity<PurchaseOrderResource> response = null;

		try {
			po = poService.cancelPO(id);
			PurchaseOrderResource res = assembler.toResource(po);
			response = new ResponseEntity<>(res, HttpStatus.OK);
		} catch (PuchasedOrderNotFound | MethodNotAllowedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			PurchaseOrderResource res = assembler.toResource(po);
			response = new ResponseEntity<>(res, HttpStatus.METHOD_NOT_ALLOWED);
		}
		return response;

	}
    
   
    @RequestMapping(method = RequestMethod.POST, value = "/modify")
	public ResponseEntity<PurchaseOrderResource> modifyPO(
			@RequestBody PurchaseOrderResource request)
			throws PuchasedOrderNotFound, MethodNotAllowedException {
		POModification pm = new POModification();
		pm.setEndDate(request.getEnd());
		pm.setStartDate(request.getStart());
		pm.setId(request.getPoId());
		PurchaseOrder po = poService.modifyPO(pm);
		

		PurchaseOrderResourceAssembler assembler = new PurchaseOrderResourceAssembler();
		ResponseEntity<PurchaseOrderResource> response = new ResponseEntity<>(
				assembler.toResource(po), HttpStatus.OK);
		return response;
	}
    
    
	@RequestMapping(method = RequestMethod.POST, value = "/extend")
	public ResponseEntity<PurchaseOrderResource> requestUpdatePO(
			@RequestBody PurchaseOrderResource update) throws JsonProcessingException {
		PurchaseOrderResourceAssembler assembler = new PurchaseOrderResourceAssembler();
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(update.getPoId());
		ResponseEntity<PurchaseOrderResource> response = null;

		try {
			po = poService.requestUpdatePO(update);
			PurchaseOrderResource res = assembler.toResource(po);
			response = new ResponseEntity<>(res, HttpStatus.OK);
		} catch (PuchasedOrderNotFound | MethodNotAllowedException e) {
			e.printStackTrace();
			PurchaseOrderResource res = assembler.toResource(po);
			response = new ResponseEntity<>(res, HttpStatus.METHOD_NOT_ALLOWED);
			RejectBean bean = new RejectBean();
			bean.setComment("Extension rejected.");
			bean.setId(po.getId());
			BuilditAPIController.rejectPHRExtension(bean);
		}
		return response;

	}

	
	@ExceptionHandler({ PlantUnavailableException.class,
			InvalidHirePeriodException.class })
	public ResponseEntity<String> handleBadRequest(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ PuchasedOrderNotFound.class,
			UpdateRequestNotFound.class })
	public ResponseEntity<String> handleNotFound(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler({ SecurityException.class })
	public ResponseEntity<String> handleSecurity(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler({ NoSuchMethodException.class })
	public ResponseEntity<String> handleNoMethod(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_IMPLEMENTED);
	}

	@ExceptionHandler({ MethodNotAllowedException.class })
	public ResponseEntity<String> handleMethodNotAllowed(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(),
				HttpStatus.METHOD_NOT_ALLOWED);
	}

}
