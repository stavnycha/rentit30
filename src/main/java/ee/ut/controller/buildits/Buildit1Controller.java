package ee.ut.controller.buildits;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import ee.ut.controller.Servers;
import ee.ut.domain.PurchaseOrder;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.RejectBean;

public class Buildit1Controller implements BuilditController {

	String server = BuilditServers.BUILDIT_1;
	
	@Override
	public void acceptPHR(Long id) {
		
	}

	@Override
	public void rejectPHR(RejectBean bean) throws JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(bean.getId());
		bean.setId(po.getHireRequestId());

		ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(bean);

		HttpEntity<String> requestEntity = new HttpEntity<String>(json, Servers.getHeaders(po
				.getCredentials()));
		RestTemplate template = new RestTemplate();

		requestEntity = new HttpEntity<String>(json, Servers.getHeaders(po
				.getCredentials()));

		String res = template.postForObject(server + "/rest/PlantHireRequests/" + po.getHireRequestId() + "/rejectByrentit30",
				requestEntity, String.class);
	}

	@Override
	public void deliverPlant(Long id) throws JsonProcessingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void returnPlant(Long id) throws JsonProcessingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void rejectPlant(Long id) throws JsonProcessingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void rejectExtensionPHR(RejectBean bean)
			throws JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(bean.getId());
		bean.setId(po.getHireRequestId());

		ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(bean);

		HttpEntity<String> requestEntity = new HttpEntity<String>(json, Servers.getHeaders(po
				.getCredentials()));
		RestTemplate template = new RestTemplate();

		requestEntity = new HttpEntity<String>(json, Servers.getHeaders(po
				.getCredentials()));

		String res = template.postForObject(server + "/rest/PlantHireRequests/" + po.getHireRequestId() + "/rejectExtensionByrentit30",
				requestEntity, String.class);
		
	}

}
