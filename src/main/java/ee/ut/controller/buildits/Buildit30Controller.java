package ee.ut.controller.buildits;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import ee.ut.controller.Servers;
import ee.ut.domain.PurchaseOrder;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.RejectBean;

public class Buildit30Controller implements BuilditController{

	String server = BuilditServers.BUILDIT_30;
	
	@Override
	public void acceptPHR(Long id) {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		HttpEntity<String> requestEntity = new HttpEntity<String>(
				Servers.getHeaders(po.getCredentials()));
		RestTemplate template = new RestTemplate();

		template.exchange(server + "/rest/requests/approve/" + po.getHireRequestId(), HttpMethod.GET, requestEntity,
				PurchaseOrderResource.class);
	}

	@Override
	public void rejectPHR(RejectBean bean) throws JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(bean.getId());
		bean.setId(po.getHireRequestId());

		ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(bean);

		HttpEntity<String> requestEntity = new HttpEntity<String>(json, Servers.getHeaders(po
				.getCredentials()));
		RestTemplate template = new RestTemplate();

		requestEntity = new HttpEntity<String>(json, Servers.getHeaders(po
				.getCredentials()));

		String res = template.postForObject(server + "/rest/requests/reject",
				requestEntity, String.class);
	}

	@Override
	public void deliverPlant(Long id) throws JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		HttpEntity<String> requestEntity = new HttpEntity<String>(
				Servers.getHeaders(po.getCredentials()));
		RestTemplate template = new RestTemplate();

		template.exchange(server + "/rest/requests/" + po.getHireRequestId() + "/inuse", // inuse
				HttpMethod.PUT, requestEntity, Void.class);
		
	}

	@Override
	public void returnPlant(Long id) throws JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		HttpEntity<String> requestEntity = new HttpEntity<String>(
				Servers.getHeaders(po.getCredentials()));
		RestTemplate template = new RestTemplate();

		template.exchange(server + "/rest/requests/" + po.getHireRequestId() + "/backtosupplier", 
				HttpMethod.PUT, requestEntity, Void.class);
	}

	@Override
	public void rejectPlant(Long id) throws JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		HttpEntity<String> requestEntity = new HttpEntity<String>(
				Servers.getHeaders(po.getCredentials()));
		RestTemplate template = new RestTemplate();

		template.exchange(server + "/rest/requests/" + po.getHireRequestId(),
				HttpMethod.DELETE, requestEntity, Void.class);
	}

	@Override
	public void rejectExtensionPHR(RejectBean bean)
			throws JsonProcessingException {
		// TODO Auto-generated method stub
		
	}

}
