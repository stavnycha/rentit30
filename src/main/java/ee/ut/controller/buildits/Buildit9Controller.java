package ee.ut.controller.buildits;

import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import ee.ut.domain.PurchaseOrder;
import ee.ut.rest.RejectBean;

public class Buildit9Controller implements BuilditController {

	String server = BuilditServers.BUILDIT_9;
	
	@Override
	public void acceptPHR(Long id) {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		
		getClient(po).resource(server + "/rest/pos" + po.getHireRequestId() + "/rentitconfirm")
                .type(MediaType.APPLICATION_XML)
                .post(ClientResponse.class);
	}

	@Override
	public void rejectPHR(RejectBean bean) {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(bean.getId());
		
		getClient(po).resource(server + "/rest/pos" + po.getHireRequestId() + "/rentitreject")
                .type(MediaType.APPLICATION_XML)
                .post(ClientResponse.class);
	}

	@Override
	public void deliverPlant(Long id) throws JsonProcessingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void returnPlant(Long id) throws JsonProcessingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void rejectPlant(Long id) throws JsonProcessingException {
		// TODO Auto-generated method stub
		
	}
	
	private static Client getClient(PurchaseOrder po) {
        Client client = Client.create();
        String user = po.getCredentials().split(":")[0], pass = po.getCredentials().split(":")[1];
        client.addFilter(new HTTPBasicAuthFilter(user, pass));
        return client;
    }

	@Override
	public void rejectExtensionPHR(RejectBean bean)
			throws JsonProcessingException {
		// TODO Auto-generated method stub
		
	}

}
