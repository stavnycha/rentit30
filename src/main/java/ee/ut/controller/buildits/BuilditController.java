package ee.ut.controller.buildits;

import java.net.URI;
import java.text.ParseException;

import javax.xml.bind.JAXBException;
import javax.xml.xpath.XPathExpressionException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import ee.ut.controller.Servers;
import ee.ut.domain.Invoice;
import ee.ut.domain.PurchaseOrder;
import ee.ut.rest.InvoiceResource;
import ee.ut.rest.PlantResource;
import ee.ut.rest.PlantResourceList;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.QueriedPlant;
import ee.ut.rest.RejectBean;

public interface BuilditController {

	public void acceptPHR(Long id);

	public void rejectPHR(RejectBean bean) throws JsonProcessingException;
	
	public void rejectExtensionPHR(RejectBean bean) throws JsonProcessingException;

	public void deliverPlant(Long id) throws JsonProcessingException;

	public void returnPlant(Long id) throws JsonProcessingException;

	public void rejectPlant(Long id) throws JsonProcessingException;
}