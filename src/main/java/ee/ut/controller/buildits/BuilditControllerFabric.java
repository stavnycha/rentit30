package ee.ut.controller.buildits;

public class BuilditControllerFabric {

	
	static public BuilditController getController(String server){
		
		BuilditController res = null;
		
		switch (server) {
			case BuilditServers.BUILDIT_30: 
				res = new Buildit30Controller();
				break;
			case BuilditServers.BUILDIT_1: 
				res = new Buildit1Controller();
				break;
			case BuilditServers.BUILDIT_9: 
				res = new Buildit9Controller();
				break;
			default: break;
		}
		
		return res;
	}
}
