package ee.ut.controller.buildits;

import java.util.ArrayList;
import java.util.List;

public class BuilditServers {

	public final static String BUILDIT_30 = //"http://localhost:8080/Rentit";//*/
			"http://buildit30.herokuapp.com";
	public final static String BUILDIT_1 = "http://buildit1.herokuapp.com";
	public final static String BUILDIT_1_EMAIL = "esi.build1@gmail.com";
	public final static String BUILDIT_30_EMAIL = "esi.buildit@gmail.com";
	public final static String BUILDIT_9 = "http://buildit9.herokuapp.com";
	public final static String BUILDIT_9_EMAIL = "buildit9esi@gmail.com";
	
	public List<String> getAllServers(){
		List<String> res = new ArrayList<>();
		res.add(BUILDIT_1);
		res.add(BUILDIT_30);
		res.add(BUILDIT_9);
		return res;
	}
	
	static public String getServerFromEmail(String email){
		String res = null;
		switch (email){
			case BUILDIT_30_EMAIL: res = BUILDIT_30; break;
			case BUILDIT_1_EMAIL: res = BUILDIT_1; break;
			case BUILDIT_9_EMAIL: res = BUILDIT_9; break;
			default:break;
		}
		return res;
	}
	
	static public String getEmailFromServer(String server){
		String res = null;
		switch (server){
			case BUILDIT_30: res = BUILDIT_30_EMAIL; break;
			case BUILDIT_1: res = BUILDIT_1_EMAIL; break;
			case BUILDIT_9_EMAIL: res = BUILDIT_9; break;
			default:break;
		}
		return res;
	}
}
