package ee.ut.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(versionField = "", table = "purchaseorder", schema = "public")
public class PurchaseOrder {

	@Column(name = "requestid")
	private Long hireRequestId;

	@Column(name = "siteid")
	private Integer siteId;

	@Column(name = "startdate")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "M-")
	private Date startDate;

	@Column(name = "enddate")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "M-")
	private Date endDate;

	@Column(name = "cost")
	private Double cost;

	@Column(name = "status")
	private POStatus status;

	@Column(name = "email")
	private String email;

	@Column(name = "server")
	private String server;

	@Column(name = "credentials")
	private String credentials;

	public String getCredentials() {
		return credentials;
	}

	public void setCredentials(String credentials) {
		this.credentials = credentials;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteid) {
		this.siteId = siteid;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startdate) {
		this.startDate = startdate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	/**
    */
	@ManyToOne
	private Plant plant;

	/**
     */
	@OneToOne
	private Invoice invoice;

}
