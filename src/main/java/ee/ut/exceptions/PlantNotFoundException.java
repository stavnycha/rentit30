package ee.ut.exceptions;

@SuppressWarnings("serial")
public class PlantNotFoundException extends Exception {
	public PlantNotFoundException(String message) {
		super(message);
	}
}
