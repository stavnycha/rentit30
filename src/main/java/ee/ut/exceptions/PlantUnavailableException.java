package ee.ut.exceptions;

@SuppressWarnings("serial")
public class PlantUnavailableException extends Exception {
	public PlantUnavailableException(String message) {
		super(message);
	}
}
