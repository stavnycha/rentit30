package ee.ut.exceptions;

@SuppressWarnings("serial")
public class PuchasedOrderNotFound extends Exception {
	public PuchasedOrderNotFound(String message) {
		super(message);
	}
}
