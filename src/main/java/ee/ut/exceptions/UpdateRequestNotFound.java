package ee.ut.exceptions;

@SuppressWarnings("serial")
public class UpdateRequestNotFound extends Exception {
	public UpdateRequestNotFound(String message) {
		super(message);
	}
}
