package ee.ut.repository;

import java.util.Date;
import java.util.List;

import ee.ut.domain.Plant;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;
import org.springframework.transaction.annotation.Transactional;

@RooJpaRepository(domainType = Plant.class)
public interface PlantRepository {

	@Query("SELECT p FROM Plant p WHERE p.title = :name AND p NOT IN (SELECT r.plant FROM PurchaseOrder r WHERE r.status <> ee.ut.domain.POStatus.REJECTED AND r.status <> ee.ut.domain.POStatus.CLOSED AND (r.startDate between :startDate and :endDate OR r.endDate between :startDate and :endDate))")
	@Transactional(readOnly = true)
	List<Plant> findByNameAndAvailability(@Param("name") String name,
			@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query("SELECT p FROM Plant p WHERE p NOT IN (SELECT r.plant FROM PurchaseOrder r WHERE r.status <> ee.ut.domain.POStatus.REJECTED AND r.status <> ee.ut.domain.POStatus.CLOSED AND (r.startDate between :startDate and :endDate OR r.endDate between :startDate and :endDate))")
	@Transactional(readOnly = true)
	List<Plant> findAvailable(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Query("SELECT p FROM Plant AS p WHERE p.id = :id AND p NOT IN (SELECT r.plant FROM PurchaseOrder r WHERE r.status <> ee.ut.domain.POStatus.REJECTED AND r.status <> ee.ut.domain.POStatus.CLOSED AND (r.startDate between :startDate and :endDate OR r.endDate between :startDate and :endDate))")
	@Transactional(readOnly = true)
	Plant findByIdAndAvailability(@Param("id") Long id,
			@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}
