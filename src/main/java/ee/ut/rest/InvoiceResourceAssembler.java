package ee.ut.rest;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import ee.ut.controller.PurchaseOrderRESTController;
import ee.ut.domain.Invoice;

public class InvoiceResourceAssembler extends
		ResourceAssemblerSupport<Invoice, InvoiceResource> {

	public InvoiceResourceAssembler() {
		super(PurchaseOrderRESTController.class, InvoiceResource.class);
	}

	@Override
	public InvoiceResource toResource(Invoice entity) {
		InvoiceResource res = createResourceWithId(entity.getId(), entity);
		res.setDate(entity.getDate());
		res.setInvoiceId(entity.getId());
		res.setPoId(entity.getPoId());
		res.setRequestId(entity.getRequestId());
		res.setStatus(entity.getStatus());
		res.setTotal(entity.getTotal());
		return res;
	}
}
