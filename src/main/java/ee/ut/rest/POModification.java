package ee.ut.rest;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.roo.addon.javabean.RooJavaBean;

@XmlRootElement
@RooJavaBean
public class POModification {
	private Long id;
	private Date startDate;
	private Date endDate;
}
