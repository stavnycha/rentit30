package ee.ut.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.roo.addon.javabean.RooJavaBean;

import ee.ut.utils.ResourceSupport;

@RooJavaBean
@XmlRootElement(name = "plant")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlantResource extends ResourceSupport {
	private Long plantId;
	private String title;
	private String description;
	private Double price;

	public Long getPlantId() {
		return plantId;
	}

	public void setPlantId(Long plantId) {
		this.plantId = plantId;
	}
}
