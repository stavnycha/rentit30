// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package ee.ut.security;

import ee.ut.security.Assignment;
import ee.ut.security.Authorities;
import ee.ut.security.Users;

privileged aspect Assignment_Roo_JavaBean {
    
    public Users Assignment.getUserrentit() {
        return this.userrentit;
    }
    
    public void Assignment.setUserrentit(Users userrentit) {
        this.userrentit = userrentit;
    }
    
    public Authorities Assignment.getAuthority() {
        return this.authority;
    }
    
    public void Assignment.setAuthority(Authorities authority) {
        this.authority = authority;
    }
    
}
