package ee.ut.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ee.ut.domain.Plant;
import ee.ut.exceptions.PlantNotFoundException;
import ee.ut.repository.PlantRepository;
import ee.ut.rest.PlantResource;

@Service
public class PlantService {

	public Plant getPlant(Long id) throws PlantNotFoundException {
		Plant p = Plant.findPlant(id);
		if (p == null)
			throw new PlantNotFoundException("Plant not found.");
		return p;
	}

	public Plant createPlant(PlantResource res) {
		Plant p = new Plant();
		p.setTitle(res.getTitle());
		p.setDescription(res.getDescription());
		p.setPrice(res.getPrice());

		p.persist();
		return p;
	}
}
