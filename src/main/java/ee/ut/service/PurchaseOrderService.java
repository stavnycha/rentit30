package ee.ut.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.fasterxml.jackson.core.JsonProcessingException;

import ee.ut.controller.BuilditAPIController;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailMessage;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import ee.ut.controller.Servers;
import ee.ut.domain.Invoice;
import ee.ut.domain.InvoiceStatus;
import ee.ut.domain.POStatus;
import ee.ut.domain.Plant;
import ee.ut.domain.PurchaseOrder;
import ee.ut.exceptions.InvalidHirePeriodException;
import ee.ut.exceptions.MethodNotAllowedException;
import ee.ut.exceptions.PlantUnavailableException;
import ee.ut.exceptions.PuchasedOrderNotFound;
import ee.ut.exceptions.UpdateRequestNotFound;
import ee.ut.repository.PlantRepository;
import ee.ut.rest.InvoiceResource;
import ee.ut.rest.POModification;
import ee.ut.rest.PurchaseOrderResource;

@Service
public class PurchaseOrderService {

	@Autowired
	PlantRepository plantRepo;

	@Autowired
	JavaMailSender mailSender;

	public PurchaseOrder createPO(PurchaseOrderResource res)
			throws PlantUnavailableException, InvalidHirePeriodException {

		if (res.getStart().compareTo(res.getEnd()) >= 0)
			throw new InvalidHirePeriodException("Invalid date range");

		List<Plant> plants;
		if (res.getPlant().getPlantId() != null) {
			plants = new ArrayList<>();
			// plants.add(plantRepo.findByIdAndAvailability(res.getPlant()
			// .getPlantId(), res.getStart(), res.getEnd()));
			plants.add(Plant.findPlant(res.getPlant().getPlantId()));
		} else {
			plants = plantRepo.findByNameAndAvailability(res.getPlant()
					.getTitle(), res.getStart(), res.getEnd());
		}

		if (!plants.isEmpty()) {

			PurchaseOrder po = new PurchaseOrder();
			po.setStatus(POStatus.PENDING_CONFIRMATION);
			po.setHireRequestId(res.getHireRequestId());
			po.setPlant(plants.get(0));
			// Calendar start = Calendar.getInstance(), end =
			// Calendar.getInstance();
			// start.setTime(res.getStart());
			// end.setTime(res.getEnd());

			po.setStartDate(res.getStart());
			po.setEndDate(res.getEnd());
			po.setSiteId(res.getSiteId());
			po.setEmail(res.getEmail());
			po.setServer(res.getServer());
			po.setCredentials(res.getCredentials());

			DateTime startDate = new DateTime(res.getStart());
			DateTime endDate = new DateTime(res.getEnd());
			int days = Days.daysBetween(startDate, endDate).getDays();

			po.setCost(days * po.getPlant().getPrice());
			po.persist();

			return po;
		} else
			throw new PlantUnavailableException(
					"The requested plant is not available");

	}

	public void sendInvoice(Long id) throws PuchasedOrderNotFound,
			FileNotFoundException, MessagingException, JAXBException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		if (po == null)
			throw new PuchasedOrderNotFound("Purchase order not found.");
		Invoice i = new Invoice();
		i.setDate(new Date());
		i.setTotal(po.getCost());
		i.setRequestId(po.getHireRequestId());
		i.setStatus(InvoiceStatus.NOT_PAID);
		i.setPoId(po.getId());
		i.persist();
		po.setInvoice(i);
		po.setStatus(POStatus.INVOICED);
		po.persist();

		// JavaMailSender mailSender;
		// MailMessage mailMessage = new SimpleMailMessage();
		MimeMessage mailMessage = mailSender.createMimeMessage();

		// mailMessage.setTo("esi.buildit@gmail.com");
		mailMessage.setSentDate(new Date());
		MimeMessageHelper helper = new MimeMessageHelper(mailMessage, true);

		helper.setFrom(Servers.email);
		helper.setTo(po.getEmail());
		helper.setSubject("Invoice");
		helper.setText("Invoice for plant order.");

		File file = new File("invoice.xml");
		
		FileOutputStream is = new FileOutputStream(file);
		JAXBContext jaxbCtx = JAXBContext.newInstance(InvoiceResource.class);

		InvoiceResource res = new InvoiceResource();
		res.setDate(i.getDate());
		res.setInvoiceId(i.getId());
		res.setPoId(i.getPoId());
		res.setRequestId(i.getRequestId());
		res.setStatus(i.getStatus());
		res.setTotal(i.getTotal());

		jaxbCtx.createMarshaller().marshal(res, is);

		helper.addAttachment(file.getName(), file);
		mailSender.send(mailMessage);
		return;
	}

	public PurchaseOrder acceptPO(Long id) throws PuchasedOrderNotFound,
			MethodNotAllowedException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		if (po == null)
			throw new PuchasedOrderNotFound("Purchase order not found.");
		if (po.getStatus() != POStatus.PENDING_CONFIRMATION)
			throw new MethodNotAllowedException("Method not allowed.");
		po.setStatus(POStatus.PENDING_PLANT_EXAMINATION);
		po.persist();
		return po;
	}

	public PurchaseOrder rejectPO(Long id) throws PuchasedOrderNotFound,
			MethodNotAllowedException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		if (po == null)
			throw new PuchasedOrderNotFound("Purchase order not found.");
		if (po.getStatus() != POStatus.PENDING_CONFIRMATION)
			throw new MethodNotAllowedException("Method not allowed.");
		po.setStatus(POStatus.REJECTED);
		po.persist();

		return po;
	}

	private PurchaseOrder closePO(Long id) throws PuchasedOrderNotFound,
			MethodNotAllowedException, JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		if (po == null) {
			throw new PuchasedOrderNotFound("Purchase order not found.");
		}
		po.setStatus(POStatus.CLOSED);
		po.persist();
		return po;
	}

	/*
	 * private PurchaseOrder changeUpdateRequestStatus(Long id, URStatus status)
	 * throws PuchasedOrderNotFound, UpdateRequestNotFound{ PurchaseOrder po =
	 * PurchaseOrder.findPurchaseOrder(id); if (po == null) throw new
	 * PuchasedOrderNotFound("Purchase order not found."); POUpdate request =
	 * po.getUpdateRequest(); if (request == null) throw new
	 * UpdateRequestNotFound
	 * ("This purchase order dotes not have update request.");
	 * po.getUpdateRequest().setStatus(status);
	 * po.setStatus(POStatus.PENGING_PLANT_EXAMINATION); po.persist(); return
	 * po; }
	 * 
	 * //TODO: change cost? public PurchaseOrder acceptUpdatePO(Long id) throws
	 * PuchasedOrderNotFound, UpdateRequestNotFound{ return
	 * changeUpdateRequestStatus(id, URStatus.ACCEPTED); } public PurchaseOrder
	 * rejectUpdatePO(Long id) throws PuchasedOrderNotFound,
	 * UpdateRequestNotFound{ return changeUpdateRequestStatus(id,
	 * URStatus.REJECTED); }
	 * 
	 * public PurchaseOrder createRequestUpdatePO(Long id, POUpdateResource res)
	 * throws PuchasedOrderNotFound, InvalidHirePeriodException{ PurchaseOrder
	 * po = PurchaseOrder.findPurchaseOrder(id); if (po == null) throw new
	 * PuchasedOrderNotFound("Purchase order not found.");
	 * 
	 * if (po.getEndDate().after(res.getDate())){ throw new
	 * InvalidHirePeriodException
	 * ("The end date of the update request is earlier then in order."); }
	 * POUpdate update = new POUpdate(); update.setStatus(URStatus.OPEN);
	 * update.setEndDate(res.getDate()); update.persist();
	 * po.setUpdateRequest(update); po.setStatus(POStatus.PENDING_UPDATE);
	 * po.persist(); return po; }
	 */

	/*
	 * public PurchaseOrder modifyPO(PurchaseOrderResource res) throws
	 * PuchasedOrderNotFound, InvalidHirePeriodException{ PurchaseOrder po =
	 * PurchaseOrder.findPurchaseOrder(res.getPoId()); if (po == null) throw new
	 * PuchasedOrderNotFound("Purchase order not found."); if
	 * (res.getStart().compareTo(res.getEnd()) >= 0) throw new
	 * InvalidHirePeriodException("Invalid date range");
	 * 
	 * po.setCost(res.getPrice()); po.setHireRequestId(res.getHireRequestId());
	 * po.setPlant(Plant.findPlant(res.getPlant().getPlantId())); //Calendar
	 * start = Calendar.getInstance(), end = Calendar.getInstance();
	 * //start.setTime(res.getStart()); //end.setTime(res.getEnd());
	 * po.setStartDate(res.getStart()); po.setEndDate(res.getEnd());
	 * po.setSiteId(res.getSiteId());
	 * po.setStatus(POStatus.PENDING_CONFIRMATION);
	 * po.setUpdateRequest(POUpdate.findPOUpdate(res.getUpdateRequest()
	 * .getUpdateId())); po.persist(); return po; }
	 */

	public PurchaseOrder getPO(Long id) throws PuchasedOrderNotFound {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		if (po == null)
			throw new PuchasedOrderNotFound("Purchase order not found.");
		return po;
	}

	public PurchaseOrder rejectPlant(Long id) throws PuchasedOrderNotFound,
			JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		if (po == null)
			throw new PuchasedOrderNotFound("Purchase order not found.");
		po.setStatus(POStatus.PLANT_REJECTED);
		po.persist();
		BuilditAPIController.rejectPlant(id);
		return po;
	}

	public PurchaseOrder receivePlant(Long id) throws PuchasedOrderNotFound,
			JsonProcessingException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		if (po == null)
			throw new PuchasedOrderNotFound("Purchase order not found.");
		po.setStatus(POStatus.PLANT_DELIVERED);
		po.persist();
		BuilditAPIController.deliverPlant(id);
		return po;
	}

	public Invoice acceptInvoice(Long id) throws JsonProcessingException,
			PuchasedOrderNotFound, MethodNotAllowedException {
		Invoice i = Invoice.findInvoice(id);
		i.setStatus(InvoiceStatus.PAID);
		i.persist();
		closePO(i.getPoId());// close the purchase order after payment.
		return i;
	}

	public PurchaseOrder modifyPO(POModification request)
			throws PuchasedOrderNotFound, MethodNotAllowedException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(request.getId());
		if (po == null)
			throw new PuchasedOrderNotFound("Purchase order not found.");
		POStatus s = po.getStatus();
		po.setStatus(POStatus.REJECTED);
		po.persist();//temporary for the reqeust in next line
		Plant plant = plantRepo.findByIdAndAvailability(po.getPlant().getId(), 
				request.getStartDate(), request.getEndDate());
		po.setStatus(s);
		po.persist();
		if (plant == null)
			throw new MethodNotAllowedException("The modification is rejected.");
		
		po.setStartDate(request.getStartDate());
		po.setEndDate(request.getEndDate());
		po.setStatus(POStatus.PENDING_PLANT_EXAMINATION);
		
		DateTime startDate = new DateTime(po.getStartDate());
		DateTime endDate = new DateTime(po.getEndDate());
		int days = Days.daysBetween(startDate, endDate).getDays();

		po.setCost(days * po.getPlant().getPrice());
		
		po.persist();
		return po;
	}

	public PurchaseOrder cancelPO(Long id) throws PuchasedOrderNotFound,
			MethodNotAllowedException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		if (po == null)
			throw new PuchasedOrderNotFound("Purchase order not found.");
		if (po.getStatus() != POStatus.PLANT_DISPATCHED
				&& po.getStatus() != POStatus.PLANT_DELIVERED
				&& po.getStatus() != POStatus.PLANT_RETURNED) {
			po.setStatus(POStatus.CANCELLED);
			po.persist();
		} else {
			throw new MethodNotAllowedException(
					"The plant is already dispatched");
		}
		return po;
	}

	public PurchaseOrder requestUpdatePO(PurchaseOrderResource update)
			throws PuchasedOrderNotFound, MethodNotAllowedException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(update.getPoId());
		if (po == null)
			throw new PuchasedOrderNotFound("Purchase order not found.");
		//if (po.getPlant().getId() != update.getPlant().getPlantId())
		//	throw new MethodNotAllowedException("Plant can't be changed.");
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(po.getEndDate()); 
		c.add(Calendar.DATE, 1);
		Date temp = c.getTime();
		
		Plant p = plantRepo.findByIdAndAvailability(update.getPlant()
				.getPlantId(), temp, update.getEnd());
		if (p != null) {
			po.setEndDate(update.getEnd());
			
			DateTime startDate = new DateTime(po.getStartDate());
			DateTime endDate = new DateTime(po.getEndDate());
			int days = Days.daysBetween(startDate, endDate).getDays();

			po.setCost(days * po.getPlant().getPrice());

			po.persist();
		} else {
			throw new MethodNotAllowedException("Plant is not available");
		}
		return po;
	}

	public PurchaseOrder dispatchPlant(Long id) throws PuchasedOrderNotFound {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		if (po == null)
			throw new PuchasedOrderNotFound("Purchase order not found.");
		po.setStatus(POStatus.PLANT_DISPATCHED);
		po.persist();
		return po;
	}

	public PurchaseOrder returnPlant(Long id) throws PuchasedOrderNotFound,
			JsonProcessingException, MethodNotAllowedException {
		PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		if (po == null)
			throw new PuchasedOrderNotFound("Purchase order not found.");
		po.setStatus(POStatus.PLANT_RETURNED);
		po.persist();
		BuilditAPIController.returnPlant(id);
		return po;
	}
}
