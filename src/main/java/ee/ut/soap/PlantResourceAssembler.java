package ee.ut.soap;

import java.util.LinkedList;
import java.util.List;

import ee.ut.domain.Plant;

public class PlantResourceAssembler {

	public PlantResourceList toResource(final List<Plant> plist) {

		PlantResourceList res = new PlantResourceList();
		List<PlantResource> plants = new LinkedList<PlantResource>();
		for (int i = 0; i < plist.size(); i++) {
			plants.add(toResource(plist.get(i)));
		}
		res.setPlants(plants);
		return res;
	}

	public PlantResource toResource(Plant p) {
		if (p == null)
			return null;
		// PlantResource res = createResourceWithId(p.getId(), p);
		PlantResource res = new PlantResource();
		res.setPrice(p.getPrice());
		res.setTitle(p.getTitle());
		res.setDescription(p.getDescription());
		res.setPlantId(p.getId());
		return res;
	}

}
