package ee.ut.soap;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.roo.addon.javabean.RooJavaBean;

@XmlRootElement
public class PlantResourceList {

	private List<PlantResource> listPlants;

	public PlantResourceList() {
		listPlants = new LinkedList<PlantResource>();
	}

	public PlantResourceList(List<PlantResource> list) {
		listPlants = list;
	}

	@XmlElement(name = "plant")
	public List<PlantResource> getPlants() {
		return this.listPlants;
	}

	public void setPlants(List<PlantResource> plants) {
		this.listPlants = plants;
	}
}
