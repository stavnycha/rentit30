package ee.ut.soap;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;

import ee.ut.domain.POStatus;
import ee.ut.utils.DateAdapter;

@RooJavaBean
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseOrderResource {

	private Long hireRequestId;
	private PlantResource plant;
	private Long poId;
	private Double price;
	private Integer siteId;
	private POStatus status;
	private Boolean paid;

	private Date startDate;

	private Date endDate;

	public Long getHireRequestId() {
		return this.hireRequestId;
	}

	public void setHireRequestId(Long hireRequestId) {
		this.hireRequestId = hireRequestId;
	}

	public PlantResource getPlant() {
		return this.plant;
	}

	public void setPlant(PlantResource plant) {
		this.plant = plant;
	}

	public Long getPoId() {
		return this.poId;
	}

	public void setPoId(Long poId) {
		this.poId = poId;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getSiteId() {
		return this.siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public POStatus getStatus() {
		return this.status;
	}

	public void setStatus(POStatus status) {
		this.status = status;
	}

	@XmlJavaTypeAdapter(DateAdapter.class)
	public Date getStart() {
		return this.startDate;
	}

	public void setStart(Date start) {
		this.startDate = start;
	}

	@XmlJavaTypeAdapter(DateAdapter.class)
	public Date getEnd() {
		return this.endDate;
	}

	public void setEnd(Date end) {
		this.endDate = end;
	}
}