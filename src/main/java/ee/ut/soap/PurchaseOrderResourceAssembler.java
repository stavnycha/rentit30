package ee.ut.soap;

import java.util.LinkedList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import ee.ut.controller.PurchaseOrderRESTController;
import ee.ut.domain.PurchaseOrder;

public class PurchaseOrderResourceAssembler {

	public PurchaseOrderResourceList toResource(final List<PurchaseOrder> plist) {

		PurchaseOrderResourceList res = new PurchaseOrderResourceList();
		List<PurchaseOrderResource> plants = new LinkedList<PurchaseOrderResource>();
		for (int i = 0; i < plist.size(); i++) {
			plants.add(toResource(plist.get(i)));
		}
		res.setOrders(plants);
		return res;
	}

	public PurchaseOrderResource toResource(PurchaseOrder p) {
		PurchaseOrderResource res = new PurchaseOrderResource();
		PlantResourceAssembler passembler = new PlantResourceAssembler();
		res.setPoId(p.getId());
		res.setSiteId(p.getSiteId());
		res.setPrice(p.getCost());
		res.setStatus(p.getStatus());
		res.setStart(p.getStartDate());
		res.setEnd(p.getEndDate());
		res.setHireRequestId(p.getHireRequestId());
		res.setPlant(passembler.toResource(p.getPlant()));
		return res;
	}

}
