package ee.ut.soap.web;

import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import ee.ut.domain.Plant;
import ee.ut.repository.PlantRepository;
import ee.ut.service.PlantService;
import ee.ut.service.PurchaseOrderService;
import ee.ut.soap.PlantResource;
import ee.ut.soap.PlantResourceAssembler;
import ee.ut.soap.PlantResourceList;

@WebService
public class PlantSOAPService {

	@Autowired
	PlantRepository plantRepo;

	@WebMethod
	public PlantResourceList getAllPlants() {
		List<Plant> pos = Plant.findAllPlants();

		PlantResourceAssembler assembler = new PlantResourceAssembler();
		PlantResourceList resList = assembler.toResource(pos);

		return resList;

	}

	@WebMethod
	public PlantResource getPlant(Long id) {
		Plant pos = Plant.findPlant(id);

		PlantResourceAssembler assembler = new PlantResourceAssembler();
		PlantResource res = assembler.toResource(pos);

		return res;

	}

	@WebMethod
	public PlantResourceList getAvailablePlants(String name, Date startDate,
			Date endDate) {
		PlantResourceAssembler assembler = new PlantResourceAssembler();

		return assembler.toResource(plantRepo.findByNameAndAvailability(name,
				startDate, endDate));
	}

}
