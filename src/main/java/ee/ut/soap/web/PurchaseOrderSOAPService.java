package ee.ut.soap.web;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;

import ee.ut.domain.Invoice;
import ee.ut.domain.InvoiceStatus;
import ee.ut.domain.POStatus;
import ee.ut.domain.Plant;
import ee.ut.domain.PurchaseOrder;
import ee.ut.exceptions.InvalidHirePeriodException;
import ee.ut.exceptions.PlantUnavailableException;
import ee.ut.repository.PlantRepository;
import ee.ut.service.PurchaseOrderService;
import ee.ut.soap.PurchaseOrderResource;

@WebService
public class PurchaseOrderSOAPService {

	@Autowired
	PlantRepository plantRepo;

	@WebMethod
	public PurchaseOrder createPO(PurchaseOrderResource res)
			throws PlantUnavailableException, InvalidHirePeriodException {

		if (res.getStart().compareTo(res.getEnd()) >= 0)
			throw new InvalidHirePeriodException("Invalid date range");

		List<Plant> plants = plantRepo.findByNameAndAvailability(res.getPlant()
				.getTitle(), res.getStart(), res.getEnd());

		if (!plants.isEmpty()) {

			PurchaseOrder po = new PurchaseOrder();
			po.setStatus(POStatus.PENDING_CONFIRMATION);

			po.setHireRequestId(res.getHireRequestId());
			po.setPlant(plants.get(0));
			po.setStartDate(res.getStart());
			po.setEndDate(res.getEnd());
			po.setSiteId(res.getSiteId());

			DateTime startDate = new DateTime(res.getStart());
			DateTime endDate = new DateTime(res.getEnd());
			int days = Days.daysBetween(startDate, endDate).getDays();

			po.setCost(days * po.getPlant().getPrice());
			po.persist();

			return po;
		} else
			throw new PlantUnavailableException(
					"The requested plant is not available");
	}

	@WebMethod
	public Invoice payInvoice(Long id) {
		Invoice i = Invoice.findInvoice(id);
		i.setStatus(InvoiceStatus.PAID);
		i.persist();
		return i;
	}
}
