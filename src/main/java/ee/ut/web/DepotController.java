package ee.ut.web;

import java.io.FileNotFoundException;

import javax.mail.MessagingException;
import javax.xml.bind.JAXBException;

import com.fasterxml.jackson.core.JsonProcessingException;

import ee.ut.domain.POStatus;
import ee.ut.domain.PurchaseOrder;
import ee.ut.exceptions.MethodNotAllowedException;
import ee.ut.exceptions.PuchasedOrderNotFound;
import ee.ut.service.PurchaseOrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/depot")
@Controller
@RooWebScaffold(path = "depot", formBackingObject = PurchaseOrder.class, delete = false, create = false, update = false)
public class DepotController {

	@Autowired
	private PurchaseOrderService poService;

	@RequestMapping(value = "/{id}/reject")
	public String rejectPlant(@PathVariable("id") Long id)
			throws PuchasedOrderNotFound, JsonProcessingException {
		poService.rejectPlant(id);
		return "redirect:/depot/" + id;
	}

	@RequestMapping(value = "/{id}/return")
	public String returnPlant(@PathVariable("id") Long id)
			throws PuchasedOrderNotFound, JsonProcessingException,
			MethodNotAllowedException, FileNotFoundException, MessagingException, JAXBException {
		poService.returnPlant(id);
		poService.sendInvoice(id);
		return "redirect:/depot/" + id;
	}

	@RequestMapping(value = "/{id}/receive")
	public String receivePlant(@PathVariable("id") Long id)
			throws PuchasedOrderNotFound, JsonProcessingException {
		poService.receivePlant(id);
		return "redirect:/depot/" + id;
	}

	@RequestMapping(value = "/{id}/dispatch")
	public String dispatchPlant(@PathVariable("id") Long id)
			throws PuchasedOrderNotFound {
		poService.dispatchPlant(id);
		return "redirect:/depot/" + id;
	}

}
