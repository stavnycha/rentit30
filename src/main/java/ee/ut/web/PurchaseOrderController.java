package ee.ut.web;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;

import ee.ut.controller.BuilditAPIController;
import ee.ut.controller.Servers;
import ee.ut.domain.POStatus;
import ee.ut.domain.Plant;
import ee.ut.domain.PurchaseOrder;
import ee.ut.exceptions.MethodNotAllowedException;
import ee.ut.exceptions.PuchasedOrderNotFound;
import ee.ut.repository.PlantRepository;
import ee.ut.rest.RejectBean;
import ee.ut.service.PurchaseOrderService;

import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;

@RequestMapping("/purchaseorders")
@Controller
@RooWebScaffold(path = "purchaseorders", formBackingObject = PurchaseOrder.class)
public class PurchaseOrderController {

	@Autowired
	PlantRepository plantRepository;

	@Autowired
	PurchaseOrderService service;

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
	public String create(@Valid PurchaseOrder purchaseOrder,
			BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest) {
		if (bindingResult.hasErrors()) {
			populateEditForm(uiModel, purchaseOrder);
			return "purchaseorders/create";
		}
		uiModel.asMap().clear();
		purchaseOrder.persist();
		return "redirect:/purchaseorders/"
				+ encodeUrlPathSegment(purchaseOrder.getId().toString(),
						httpServletRequest);
	}

	@RequestMapping(params = "form", produces = "text/html")
	public String createForm(Model uiModel) {
		populateEditForm(uiModel, new PurchaseOrder());
		return "purchaseorders/create";
	}

	@RequestMapping(value = "/{id}", produces = "text/html")
	public String show(@PathVariable("id") Long id, Model uiModel) {
		addDateTimeFormatPatterns(uiModel);
		uiModel.addAttribute("purchaseorder",
				PurchaseOrder.findPurchaseOrder(id));
		uiModel.addAttribute("itemId", id);
		return "purchaseorders/show";
	}

	@RequestMapping(produces = "text/html")
	public String list(
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "size", required = false) Integer size,
			Model uiModel) {
		if (page != null || size != null) {
			int sizeNo = size == null ? 10 : size.intValue();
			final int firstResult = page == null ? 0 : (page.intValue() - 1)
					* sizeNo;
			uiModel.addAttribute("purchaseorders",
					PurchaseOrder.findPurchaseOrderEntries(firstResult, sizeNo));
			uiModel.addAttribute("rejectBean", new RejectBean());
			float nrOfPages = (float) PurchaseOrder.countPurchaseOrders()
					/ sizeNo;
			uiModel.addAttribute(
					"maxPages",
					(int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1
							: nrOfPages));
		} else {
			uiModel.addAttribute("purchaseorders",
					PurchaseOrder.findAllPurchaseOrders());
		}
		addDateTimeFormatPatterns(uiModel);
		return "purchaseorders/list";
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
	public String update(@Valid PurchaseOrder purchaseOrder,
			BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest) {
		if (bindingResult.hasErrors()) {
			populateEditForm(uiModel, purchaseOrder);
			return "purchaseorders/update";
		}
		uiModel.asMap().clear();
		purchaseOrder.merge();
		return "redirect:/purchaseorders/"
				+ encodeUrlPathSegment(purchaseOrder.getId().toString(),
						httpServletRequest);
	}

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
	public String updateForm(@PathVariable("id") Long id, Model uiModel) {
		populateEditForm(uiModel, PurchaseOrder.findPurchaseOrder(id));
		return "purchaseorders/update";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        PurchaseOrder purchaseOrder = PurchaseOrder.findPurchaseOrder(id);
        purchaseOrder.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/purchaseorders?page=1&size=50";
    }

	@RequestMapping(method = RequestMethod.GET, produces = "text/html", value="/sendInvoice/{id}")
    public String sendInvoice(@PathVariable Long id) throws PuchasedOrderNotFound, MethodNotAllowedException, FileNotFoundException, MessagingException, JAXBException {
		service.sendInvoice(id);
		return "redirect:/purchaseorders?page=1&size=50";
    }
	
	@RequestMapping(method = RequestMethod.GET, produces = "text/html", value="/{id}/accept")
    public String accept(@PathVariable Long id) throws PuchasedOrderNotFound, MethodNotAllowedException {
		
		//PurchaseOrder po = PurchaseOrder.findPurchaseOrder(id);
		BuilditAPIController.acceptPHR(id);
		service.acceptPO(id);
		return "redirect:/purchaseorders/?page=1&size=50";
	}

	@RequestMapping(method = RequestMethod.POST, produces = "text/html", value="/{id}/reject")
    public String reject(@PathVariable Long id, @Valid RejectBean bean) throws PuchasedOrderNotFound, MethodNotAllowedException, JsonProcessingException {

		service.rejectPO(bean.getId());
		// bean.setId(PurchaseOrder.findPurchaseOrder(bean.getId()).getHireRequestId());
		BuilditAPIController.rejectPHR(bean);
		return "redirect:/purchaseorders?page=1&size=50";
	}

	
	void addDateTimeFormatPatterns(Model uiModel) {
		uiModel.addAttribute(
				"purchaseOrder_startdate_date_format",
				DateTimeFormat.patternForStyle("M-",
						LocaleContextHolder.getLocale()));
		uiModel.addAttribute(
				"purchaseOrder_enddate_date_format",
				DateTimeFormat.patternForStyle("M-",
						LocaleContextHolder.getLocale()));
	}

	void populateEditForm(Model uiModel, PurchaseOrder purchaseOrder) {
		uiModel.addAttribute("purchaseOrder", purchaseOrder);
		addDateTimeFormatPatterns(uiModel);
		uiModel.addAttribute("postatuses", Arrays.asList(POStatus.values()));
		uiModel.addAttribute("plants", plantRepository.findAll());
	}

	String encodeUrlPathSegment(String pathSegment,
			HttpServletRequest httpServletRequest) {
		String enc = httpServletRequest.getCharacterEncoding();
		if (enc == null) {
			enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
		}
		try {
			pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
		} catch (UnsupportedEncodingException uee) {
		}
		return pathSegment;
	}
}
