package ee.ut.web;

import java.util.List;

import javax.validation.Valid;

import ee.ut.domain.Plant;
import ee.ut.repository.PlantRepository;
import ee.ut.rest.QueriedPlant;

import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/plantlist")
@Controller
@RooWebScaffold(path = "plantlist", formBackingObject = Plant.class, delete = false, create = false, update = false)
public class UserPlantsController {
	
	@Autowired
	PlantRepository plantRepository;
	
	@RequestMapping(value = "/check", produces = "text/html")
    public String checkAvailability(Model uiModel) {
		addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("plantquery", new QueriedPlant());
        return "plantlist/check";
    }
	
	@RequestMapping(value = "/check", produces = "text/html", method = RequestMethod.POST)
    public String showAvailability(@Valid QueriedPlant plant, Model uiModel) {
		List<Plant> plants = plantRepository.findByNameAndAvailability(plant.getName(), plant.getStartDate(), plant.getEndDate());
		if (!plants.isEmpty()){
			uiModel.addAttribute("result", "Plants are available");
		} else {
			uiModel.addAttribute("result", "Plants are not available");
		}
		addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("plantquery", new QueriedPlant());
        return "plantlist/check";
    }
	
	void addDateTimeFormatPatterns(Model uiModel) {
		uiModel.addAttribute(
				"planthirerequest_startdate_date_format",
				DateTimeFormat.patternForStyle("M-",
						LocaleContextHolder.getLocale()));
		uiModel.addAttribute(
				"planthirerequest_enddate_date_format",
				DateTimeFormat.patternForStyle("M-",
						LocaleContextHolder.getLocale()));
	}
	
}
