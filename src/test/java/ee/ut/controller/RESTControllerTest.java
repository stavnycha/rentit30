package ee.ut.controller;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import ee.ut.domain.POStatus;
import ee.ut.domain.Plant;
import ee.ut.repository.PlantRepository;
import ee.ut.rest.PlantResource;
import ee.ut.rest.PlantResourceAssembler;
import ee.ut.rest.PlantResourceList;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.PurchaseOrderResourceList;
import ee.ut.service.PlantService;
import ee.ut.web.PurchaseOrderController;

public class RESTControllerTest {

	// private PlantRESTController restController = new PlantRESTController();
	private String server = "http://rentit30.herokuapp.com";// "http://localhost:8080/Rentit";

	@Test
	public void getAllPlants() {
		org.junit.Assert.assertTrue(true);
	}

	@Test
	public void createPlantResource() {
		Client client = Client.create();
		WebResource webResource = client.resource(server + "/rest/plants");
		PlantResourceList initialPlantList = webResource
				.type(MediaType.APPLICATION_XML)
				.accept(MediaType.APPLICATION_XML).get(ClientResponse.class)
				.getEntity(PlantResourceList.class);

		PlantResource resource = new PlantResource();
		resource.setTitle("Truck");
		resource.setDescription("Short description");
		resource.setPrice(200.);

		ClientResponse response = webResource.type(MediaType.APPLICATION_XML)
				.accept(MediaType.APPLICATION_XML)
				.post(ClientResponse.class, resource);

		assertTrue(response.getStatus() == ClientResponse.Status.CREATED
				.getStatusCode());

		PlantResourceList eventualPlantList = webResource
				.type(MediaType.APPLICATION_XML)
				.accept(MediaType.APPLICATION_XML).get(ClientResponse.class)
				.getEntity(PlantResourceList.class);
		assertTrue(initialPlantList.getPlants().size() + 1 == eventualPlantList
				.getPlants().size());
	}

	@Test
	public void createPurchaseOrderResource() {
		Client client = Client.create();
		WebResource webResource = client.resource(server + "/rest/po");
		PurchaseOrderResourceList initialPOList = webResource
				.type(MediaType.APPLICATION_XML)
				.accept(MediaType.APPLICATION_XML).get(ClientResponse.class)
				.getEntity(PurchaseOrderResourceList.class);

		PurchaseOrderResource resource = new PurchaseOrderResource();
		PlantResourceAssembler assembler = new PlantResourceAssembler();

		resource.setPlant(assembler.toResource(Plant.findPlant(2L)));
		resource.setHireRequestId(1L);
		resource.setPrice(400.);
		resource.setStatus(POStatus.PENDING_CONFIRMATION);
		resource.setSiteId(130);
		resource.setStart(new GregorianCalendar(2013, 4, 4).getTime());
		resource.setEnd(new GregorianCalendar(2013, 5, 4).getTime());

		ClientResponse response = webResource.type(MediaType.APPLICATION_XML)
				.accept(MediaType.APPLICATION_XML)
				.post(ClientResponse.class, resource);

		assertTrue(response.getStatus() == ClientResponse.Status.CREATED
				.getStatusCode());

		PurchaseOrderResourceList eventualPOList = webResource
				.type(MediaType.APPLICATION_XML)
				.accept(MediaType.APPLICATION_XML).get(ClientResponse.class)
				.getEntity(PurchaseOrderResourceList.class);
		assertTrue(initialPOList.getOrders().size() + 1 == eventualPOList
				.getOrders().size());
	}

	@Test
	public void cancelPurchaseOrderResource() {
		Client client = Client.create();
		WebResource webResource = client.resource(server + "/rest/po");
		PurchaseOrderResourceList initialPOList = webResource
				.type(MediaType.APPLICATION_XML)
				.accept(MediaType.APPLICATION_XML).get(ClientResponse.class)
				.getEntity(PurchaseOrderResourceList.class);

		PurchaseOrderResource tocancel = initialPOList.getOrders().get(
				initialPOList.getOrders().size() - 1);
		webResource = client.resource(server + "/rest/po/" + tocancel.getPoId()
				+ "/accept");
		ClientResponse response = webResource.type(MediaType.APPLICATION_XML)
				.accept(MediaType.APPLICATION_XML).delete(ClientResponse.class);

		assertTrue(response.getStatus() == ClientResponse.Status.OK
				.getStatusCode());

		webResource = client.resource(server + "/rest/po");
		PurchaseOrderResourceList eventualPOList = webResource
				.type(MediaType.APPLICATION_XML)
				.accept(MediaType.APPLICATION_XML).get(ClientResponse.class)
				.getEntity(PurchaseOrderResourceList.class);
		assertTrue(initialPOList.getOrders().size() - 1 == eventualPOList
				.getOrders().size());
	}

	/*
	 * @Test public void modifyPurchaseOrderResource() { Client client =
	 * Client.create(); WebResource webResource = client.resource(server +
	 * "/rest/po"); PurchaseOrderResourceList initialPOList =
	 * webResource.type(MediaType.APPLICATION_XML)
	 * .accept(MediaType.APPLICATION_XML
	 * ).get(ClientResponse.class).getEntity(PurchaseOrderResourceList.class);
	 * 
	 * PurchaseOrderResource tomodify = initialPOList.getOrders().get(0);
	 * 
	 * Long objId = tomodify.getPoId();
	 * 
	 * Double prevPrice = tomodify.getPrice(); tomodify.setPrice(prevPrice +
	 * 100.); ClientResponse response =
	 * webResource.type(MediaType.APPLICATION_XML)
	 * .accept(MediaType.APPLICATION_XML).put(ClientResponse.class, tomodify);
	 * 
	 * assertTrue(response.getStatus() ==
	 * ClientResponse.Status.OK.getStatusCode());
	 * 
	 * webResource = client.resource(server + "/rest/po/" + objId);
	 * PurchaseOrderResource eventualPO =
	 * webResource.type(MediaType.APPLICATION_XML)
	 * .accept(MediaType.APPLICATION_XML
	 * ).get(ClientResponse.class).getEntity(PurchaseOrderResource.class);
	 * assertTrue(prevPrice + 100 == eventualPO.getPrice()); }
	 */
}
